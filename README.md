# CS3323_FA22_LAB01_TEAM_17

Author(s):

Team 17 Lab 1 \
Minh Tran \
Garret Busey \
Micheal Nguyen

# Part 1

## Function

run the `make` command in the terminal \
Given the test file named `"test1.in"` you can then type `./pl-scanner.exe < test1.in` into the commandline for successful cases. \
Given the test file named `"test1.in"` you can then type `./pl-scanner.exe < test2.in` into the commandline for unsuccessful cases.

## Division of Work:

Garret Busey: Test cases, assist in task 3, README\
Minh Tran: Task 1, assist in task 2, README \
Micheal Nguyen: Task 3, assist in task 2, README

## Schedules:

Work on being able to connect to GitLab, Get in contact \
Micheal: Upload project onto GitLab \
Garret, Minh: Get GitLab connectiont to work \
`11/7 GROUP MEETING` : 1 hour \
Begin work on tasks \
Minh: Task 1 \
Micheal: Task 3 \
Garret: Task 2 \
`11/11 GROUP MEETING` : 1 hour 30 minutes \
Continue work on Tasks \
Minh, Garret: Task 2 \
Micheal: Task 3 \
`11/14 GROUP MEETING` : 1 hour \
Continue work \
Garret: Test cases \
Minh, Micheal, Garret: finalize README \
`11/16 GROUP MEETING` : 1 hour \
Garret: Success and Fail test cases \
Minh, Micheal: fix unexpected character error \
`11/18 GROUP MEETING` : 1 hour

## Minutes:

`11/7 GROUP MEETING` \
get ssh and connection to work, unzipping and uploading project to GitLab

<hr \>

`11/11 GROUP MEETING` \
Begin work on task 1, 3; reporting work on tasks and discussion on tasks 2 and 3 RegEx, finished task 1 and 3.d

<hr \>

`11/14 GROUP MEETING` \
Discuss problems in task 3; completed task 3 and 2; work on README and discuss test cases

<hr \>

`11/16 GROUP MEETING` \
Discuss testfile cases; work on README and discuss grading schedule

<hr \>

`11/18 GROUP MEETING` \
Discuss unexpected characters; work on README; discuss peer evaluations

# Part 2

## Function

run the `make` command in the terminal \
run the tests individually with `./simple.exe < "filename"`.

## Division of Work:

Garret Busey: tasks 1 and 2, README\
Minh Tran: tasks 3 and 4, README\
Micheal Nguyen: tasks 5 and 6, README

## Schedules:

`GROUP MEETING 1` : 1 hour \
Continue on Work: \
Garret: tasks 1 and 2 \
Minh: tasks 3 and 4 \
Micheal: tasks 5 and 6 \
`GROUP MEETING 2` : 1 hour \
Garret, Minh, Micheal: if/else \
Micheal: task 6 and foreach \
`GROUP MEETING 3` : 1 hour

## Minutes:

`11/21 GROUP MEETING 1` \
Division of tasks and general discussion of how to proceed with lab; Submission of part 1

<hr \>

`11/28 GROUP MEETING 2` \
Peer grading, Continue work on part 2, updated README

<hr \>

`11/30 GROUP MEETING 2` \
updated README, fixed foreach (adding ID and IN), ran tests, fixed ifelse (changed if condition to have two statements instead of else)

<hr \>

# Part 3

## Function

run the `make` command in the terminal \

## Division of Work:

Garret Busey: tasks 1 and 2, README\
Minh Tran: tasks 3 and 4, README\
Micheal Nguyen: tasks 5 and 6, README

## Schedules:

`GROUP MEETING 1` : 1 hour \

## Minutes:

`12/7 GROUP MEETING 1` \
Work on our respective parts, determine the functionality of float instructions, update README

<hr \>
